import * as config from "./config";
import texts from "../data.js";

const usernames = new Set;
const rooms = {};


const sortObject = obj => {
  var arr = [];
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      arr.push({
        'key': prop,
        'value': obj[prop]
      });
    }
  }
  arr.sort(function (a, b) { return b.value - a.value; });

  return arr;
}



const greeting = 'На вулиці зараз трохи пасмурно, але на Львів Арена зараз просто чудова атмосфера: двигуни гарчать, глядачі посміхаються а гонщики ледь помітно нервують та готуюуть своїх залізних конів до заїзду. А коментувати це все дійство Вам буду я, Ескейп Ентерович і я радий вас вітати зі словами Доброго Вам дня панове!';

let cars = {
  'Ferrari': 'на червоному Ferrari',
  'McLaren': 'на жовтому McLaren',
  'Mercedes': 'на срібному Mercedes'
}

cars = new Proxy(cars, {
  get(target, phrase) {
    if (phrase in target) {
      return target[phrase]
    } else {
      return 'нажаль марка цього автомобіля мені не відома'
    }
  }
})

class Commentator {
  constructor() {
    this.finishedUsers = 0;
    this.timeFinished = [];
  }

  create({ type, carNames, roomName, amountOfRaces }) {
    let commentator
    if (type === 'funny') {
      commentator = new Funny();
    } else if (type === 'serious') {
      commentator = new Serious();
    }
    commentator.usernames = Object.keys(rooms[roomName].users);
    commentator.carNames = carNames;
    commentator.amountOfRaces = amountOfRaces;
    commentator.roomName = roomName;
    commentator.sayPresent = () => {
      let phrase = 'А тепер давайте я представлю вам учасників:';
      for (let i = 0; i < commentator.usernames.length; i++) {
        phrase += `${commentator.usernames[i]} ${cars[commentator.carNames[i]]} який провів ${commentator.amountOfRaces[i]} гонки`

        if (i + 1 < commentator.usernames.length) {
          phrase += ','
        } else {
          phrase += '.'
        }
      }
      return phrase
    }
    commentator.sayRandom = () => {
      const randomInt = Math.floor(Math.random() * Math.floor(commentator.random.length));
      return commentator.random[randomInt];
    }
    commentator.sayCurrentSituation = () => {
      const roomName = commentator.roomName;
      let text = '';
      const list = {}
      Object.keys(rooms[roomName]['users']).forEach((key) => {
        list[key] = rooms[roomName].users[key].progress;
      });
      const leaderBoard = sortObject(list);

      let currentPlace = 1;
      let previousValue;
      leaderBoard.forEach((elem) => {
        const isDistanceBetweenPreviousPlayerLow = elem.value / previousValue > 0.9;
        const isDistanceBetweenPreviousPlayerHigh = elem.value / previousValue < 0.6;
        if (currentPlace === 1) {
          text += `На даний момент лідирує ігрок ${elem.key}`;
        } else if (leaderBoard.length === currentPlace && currentPlace !== 1) {
          text += `,і замикає нашу гонку ігрок ${elem.key}.`
        } else if (currentPlace >= 2 && isDistanceBetweenPreviousPlayerLow) {
          text += `, йому на п'яти наступає ігрок ${elem.key} тим самим все ще має шанси взяти #${currentPlace - 1} місце`
        } else if (currentPlace >= 2 && isDistanceBetweenPreviousPlayerHigh) {
          text += `, не втрачаючи надію з великим відставанням ${elem.key} займає #${currentPlace}`
        } else {
          text += `,${elem.key} займає #${currentPlace}`
        }
        previousValue = elem.value;
        currentPlace++
      })
      return text

    }
    commentator.closeToFinish = (username) => {
      return `${username} наближається до фінішу`
    }
    commentator.finished = (username, raceTime) => {
      let text = ''
      this.timeFinished[username] = raceTime;
      this.finishedUsers++
      if (this.finishedUsers === 1) {
        text = `І в нас є переможець,ним став ${username}`
      } else if (this.finishedUsers === 2) {
        text = `Срібним призером стає ${username}`
      } else if (this.finishedUsers === 3) {
        text = `І останнє призове місце займає ${username}`
      } else {
        text = `${username} приходить ${this.finishedUser}`
      }

      return text
    }
    commentator.getListOfWinners = () => {
      let text = '';
      let place = 1;
      const keys = Object.keys(this.timeFinished)
      for (let i = 0; i < keys.length; i++) {
        text += `#${place} місце з часом ${this.timeFinished[keys[i]]}ms займає ігрок ${keys[i]}`;
        if (place === 3) {
          break
        }
        place++
      }
      return text
    }
    commentator.isAllUsersFinished = () => {
      return this.finishedUsers === commentator.usernames.length
    }

    return commentator
  }

}


class Funny {
  constructor() {
    this.random = ['600ккал в середньому саме стільки втрачає пілот під час кожного Гран Прі,цікаво чи спали хоча б 1 ккал наші учасники',
      'Під час жарких Гран Прі командою і її гостями випивається 3 300 літрів води і безалкольних напоїв',
      'Гонка озброєнь - це коли ти купив дриль, а сусід, перфоратор',
      'Інспектор ДАІ, професіонал своєї справи, не може спокійно дивитися гонки Формули - 1: - Від такого перевищення швидкості його може вистачити інфаркт! ',
      "Ядерна гонка озброєнь - це все одно, як два мужика, що стоять по пояс у бензині. У одного три сірники, в іншого п'ять  ..."]
  }
}

class Serious {
  constructor() {
    this.random = ['Перші гонки на справжніх автомобілях відбулися у Франції в 1894 році. Учасники повинні були проїхати 126 кілометрів від Парижа до Руана, а потім повернутися назад. І знаєш, скільком автомобілістам з більш ніж ста вдалося фінішувати? Тільки 21 щасливчику!',
      '1200 літрів палива спалюється болідами однієї команди за гоночний уїк-енд, в добавок до цієї цифри в витрата списується приблизно 70 літрів моторного масли і до 30 літрів трансмісійного масла.',
      '3 100 раз пілоти перемикають передачі під час Гран Прі Монако,цікаво а скільки раз переключили розкладку наші учасники']
  }
}

const commentatorFactory = new Commentator();

class Reset {
  resetReady(room) {
    Object.keys(rooms[room].users).map(key => {
      rooms[room].users[key].ready = false;
    })
  }

  resetProgress(room) {
    Object.keys(rooms[room].users).map(key => {
      rooms[room].users[key].progress = 0;
    })
  }

}

class ResetFacade {
  constructor(reset){
    this.reset = reset;
  }

  resetAll(room){
    this.reset.resetReady(room);
    this.reset.resetProgress(room)
  }
}

const reset = new ResetFacade(new Reset());





const joinRoom = (room, username) => {
  rooms[room].users[username] = {
    user: username,
    ready: false,
    progress: 0
  }
}
const leaveRoom = (room, username) => {
  delete rooms[room].users[username];
  if ((Object.keys(rooms[room].users).length) === 0) {
    delete rooms[room]
  }
}

const readyBtnTrue = (room, username) => {
  rooms[room].users[username].ready = true
}

const readyBtnFalse = (room, username) => {
  rooms[room].users[username].ready = false
}

const updateProgress = (room, username, progress) => {
  rooms[room].users[username].progress = progress;
}

const playerFinished = (room, username) => {
  rooms[room].finished.push(username)
}

const isAllPlayersFinised = (room) => {
  return Object.keys(rooms[room].users).every(key => rooms[room].users[key].progress === 100)
}

const showWinnersTable = (room) => {
  let results = rooms[room].finished;
  return results
}


const isAllPlayersReady = (room) => {
  return Object.keys(rooms[room].users).every(key => rooms[room].users[key].ready === true)
}



const isUserLogged = username => usernames.has(username);
const addUser = username => usernames.add(username);
const removeUser = username => usernames.delete(username);
const isUniqueRoomName = roomName => !(roomName in rooms);
let kick = false;


export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    socket.emit("IS_USER_LOGGED", isUserLogged(username));

    if (!isUserLogged(username)) {
      addUser(username)
    } else {
      kick = true
    }

    socket.emit('RENDER_LISTS_OF_ROOMS', rooms);
    socket.join('lobby');

    socket.on('CREATE', (roomName, roomObj) => {
      const isUniq = isUniqueRoomName(roomName);

      if (isUniq) {
        rooms[roomName] = roomObj
        socket.leave('lobby');
        socket.join(roomName);
        socket.emit("RENDER_GAME", rooms[roomName])
        socket.emit('COMMENTATOR_UPDATE_TEXT', greeting);
        io.in('lobby').emit("RENDER_LISTS_OF_ROOMS", rooms);
      } else {
        socket.emit('NOT_UNIQUE_ROOM_NAME');
      }
    })


    socket.on('JOIN_LOBBY', (roomName, username) => {
      socket.leave('lobby');
      joinRoom(roomName, username);
      socket.join(roomName);
      io.in(roomName).emit("RENDER_GAME", rooms[roomName]);
      socket.emit('COMMENTATOR_UPDATE_TEXT', greeting);
    })

    socket.on('LEAVE_LOBBY', (roomName, username) => {
      leaveRoom(roomName, username);
      socket.leave(roomName);
      socket.join('lobby');
      if (rooms[roomName] === undefined) {
        io.in('lobby').emit('RENDER_LISTS_OF_ROOMS', rooms);
      }
      io.in(roomName).emit("RENDER_GAME", rooms[roomName]);
    })

    socket.on('READY_BUTTON', (roomName, username) => {
      readyBtnTrue(roomName, username);
      if (isAllPlayersReady(roomName)) {

        const randomNumber = Math.floor(Math.random() * Math.floor(texts.texts.length));
        io.in(roomName).emit("START", config.SECONDS_TIMER_BEFORE_START_GAME, randomNumber, config.SECONDS_FOR_GAME);
      }
      io.in(roomName).emit("UPDATE_GAME", rooms[roomName].users);
    })

    socket.on('NOT_READY_BUTTON', (roomName, username) => {
      readyBtnFalse(roomName, username);
      io.in(roomName).emit("UPDATE_GAME", rooms[roomName].users);
    })

    socket.on('UPDATE_PROGRESS', (roomName, username, progress) => {
      updateProgress(roomName, username, progress);
      io.in(roomName).emit("UPDATE_GAME", rooms[roomName].users);
      if (progress === 100) {
        playerFinished(roomName, username);
        if (isAllPlayersFinised(roomName)) {
          const res = showWinnersTable(roomName)
          io.in(roomName).emit("WINNER", res)
        }
      }

    })

    socket.on('TIME_LEFT', (roomName) => {
      const res = showWinnersTable(roomName)
      io.in(roomName).emit("WINNER", res)
    })

    socket.on('RESET_ALL', (roomName) => {
      reset.resetAll(roomName);
      io.in(roomName).emit("UPDATE_GAME", rooms[roomName].users)
    })

    socket.on('CREATE_COMMENTATOR', ({ roomName, carNames, amountOfRaces }) => {
      const commentator = commentatorFactory.create({ type: 'funny', roomName, usernames, carNames, amountOfRaces });
      rooms[roomName].commentator = commentator;
    })

    socket.on('COMMENTATOR_GREETINGS', (roomName) => {
      io.in(roomName).emit('COMMENTATOR_UPDATE_TEXT', rooms[roomName].commentator.sayPresent());
    })

    socket.on('COMMENTATOR_RANDOM', (roomName) => {
      socket.emit('COMMENTATOR_UPDATE_TEXT', rooms[roomName].commentator.sayRandom());
    })

    socket.on('COMMENTATOR_CURRENT_SITUATION', roomName => {
      socket.emit('COMMENTATOR_UPDATE_TEXT', rooms[roomName].commentator.sayCurrentSituation(roomName));
    })

    socket.on('COMMENTATOR_30CHARS', (roomName, username) => {
      io.in(roomName).emit('COMMENTATOR_UPDATE_TEXT', rooms[roomName].commentator.closeToFinish(username));
    })

    socket.on('COMMENTATOR_FINISHED', (roomName, username, raceTime) => {
      io.in(roomName).emit('COMMENTATOR_UPDATE_TEXT', rooms[roomName].commentator.finished(username, raceTime));
      if (rooms[roomName].commentator.isAllUsersFinished()) {
        io.in(roomName).emit('COMMENTATOR_UPDATE_TEXT', rooms[roomName].commentator.getListOfWinners());
      }
    })


    socket.on("disconnect", () => {
      if (kick === false) {
        removeUser(username);
      } else {
        kick = false;
      }
    });

  });
};


